//
//  ChatEntry.swift
//  Loyality
//
//  Created by Vlad on 7/9/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import CoreLocation
import ObjectMapper

class ChatEntry: NSObject {
    
    
    var id: Int = 0
    var text: String = ""
    var dateAndTime: String = ""
    var toUserId: String = ""
    var fromUserId: String = ""
    var buttonUrl: String = ""
    var buttonText: String? = nil
    var imageUrl: String? = nil
    var taskType: String = ""
    var taskCoastType: Int = 0
    var taskCost: Int = 0
    var questions: String? = nil
    var taskId: Int = 0
    var status: Int = 0
    var answer: String = ""
    var answer2: String = ""
    var avatar: String? = nil
    var isONE: Bool = false
    var date: Date = Date()
    var list = [ChatEntry]()
    
    
}
