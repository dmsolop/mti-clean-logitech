//
//  Map.swift
//  Loyality
//
//  Created by Denis Romashov on 4/10/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import CoreLocation
import ObjectMapper

struct MapObject: Mappable {
    
    var id: Int = 0
    var x: Double = 0
    var y: Double = 0
    var action: String = ""
    var partnerID: Int = 0
    
    func coordinates() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: x, longitude: y)
    }
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        id <- map["MAP_ID"]
        partnerID <- map["PARTNER_ID"]
        x <- map["X"]
        y <- map["Y"]
        action <- map["ACTION"]
    }
}
