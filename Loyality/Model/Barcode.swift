//
//  Barcode.swift
//  Loyality
//
//  Created by Denis Romashov on 4/7/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import ObjectMapper

struct Barcode: Mappable {
    var balance: Int = 0
    var barCode: String = ""
    var type: String = ""
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        barCode <- map["BAR_CODE"]
        type <- map["BAR_CODE_TYPE"]
        balance <- map["BALANCE"]
    }
}
