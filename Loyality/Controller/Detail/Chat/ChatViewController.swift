//
//  ChatViewController.swift
//  Loyality
//
//  Created by Dmytro Solop on 7/4/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    
    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var localeBtn: UIButton!
    
    var pickImageCallback: ((UIImage) -> ())?
    var getTextCallback: ((String, String) -> ())?
    
    var isFirst = true
    private var messages = [ChatEntry]() {
        didSet {
            if isFirst {
                tableView.reloadData()
                isFirst = false
            }
        }
    }
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set up checkin button
        if Session.isCheckin {
            localeBtn.isEnabled = false
            self.localeBtn.setImage(UIImage(named: "green"), for: .disabled)
        } else {
            localeBtn.isEnabled = true
            localeBtn.imageView?.image = UIImage(named: "yellow")
        }

        chatHistiry()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addNotificationCenter()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Notifications
    func addNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(showHideSubcells(_:)),
                                       name: .showHideSubcells,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(showActionSheet(_:)),
                                       name: .showActionSheet,
                                       object: nil)
        
    }
    
    //MARK: - Actions
    
    @IBAction func chatHistoryDidPush(_ sender: UIButton) {
        chatHistiry()
    }
    
    @IBAction func back_action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func localeBtnDidPush(_ sender: UIButton) {
        self.getScanerResult({ text, type  in
            if type.contains("QRCode"), !Session.isCheckin {
                Communicator.checkIn(phone: Session.phone!, qrCode: text, completion: { [unowned self] (result) in
                    if (result["STATUS"] as! String).contains("OK") {
                        DispatchQueue.main.async {
                            self.localeBtn.setImage(UIImage(named: "green"), for: .disabled)
                            self.localeBtn.isEnabled = false
                            self.view.layoutIfNeeded()
                        }
                        Session.isCheckin = true
                    }
                    self.chatHistiry(withDelay: 2)
                })
            }
        })
    }
    
    //MARK: - Requests
    fileprivate func chatHistiry(withDelay delay: UInt32 = 0) {
        sleep(delay)
        Communicator.chatHistory(phone: Session.phone!) { [unowned self] (response) in
            self.messages = response
            self.tableView.reloadData()
            if let indexPath = Session.indexPath {
                self.tableView.scrollToCurrent(indexPath)
            } /*else {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToCurrent(indexPath)
            }*/
            Session.indexPath = nil
            Session.listIndexPaths = []
        }

    }
    
    //MARK: - Attached messages
    @objc func showHideSubcells(_ notification: NSNotification) {
        guard let dict = notification.userInfo as NSDictionary?,
            var indexPath = dict["indexPath"] as? IndexPath,
            let isShow = dict["isShow"] as? Bool,
            let subMessages = dict["subMessages"] as? [ChatEntry] else {return}
        var listIndexPaths = [IndexPath]()
        
        if let savedIndexPath = Session.indexPath, indexPath != savedIndexPath && isShow {
            self.removeSubcells(indexPath: savedIndexPath, deleteIPs: &Session.listIndexPaths)
            if indexPath.row > savedIndexPath.row {
                indexPath.row = indexPath.row - Session.listIndexPaths.count
            }
        }
        
        if isShow {
            self.addSubcells(submessages: subMessages, indexPath: indexPath, insertIPs: &listIndexPaths)
        } else {
            self.removeSubcells(submessages: subMessages, indexPath: indexPath, deleteIPs: &listIndexPaths)
        }
        self.tableView.reloadData()
        self.tableView.scrollToCurrent(indexPath)
    }
    
    func addSubcells(submessages:[ChatEntry], indexPath: IndexPath, insertIPs: inout [IndexPath]) {
        for (index, item) in submessages.enumerated() {
            messages.insert(item, at: indexPath.row + index + 1)
            let indexPath = IndexPath(row: indexPath.row + index + 1, section: 0)
            insertIPs.append(indexPath)
        }
        self.tableView.insertRows(at: insertIPs, with: .automatic)
        
        Session.listIndexPaths = insertIPs
        Session.indexPath = indexPath
    }
    
    func removeSubcells(submessages:[ChatEntry], indexPath: IndexPath, deleteIPs: inout [IndexPath]) {
        for (index, _) in submessages.enumerated() {
            messages.remove(at: indexPath.row + 1)
            let indexPath = IndexPath(row: indexPath.row + index + 1, section: 0)
            deleteIPs.append(indexPath)
        }
        self.tableView.deleteRows(at: deleteIPs, with: .automatic)
        
        Session.indexPath = nil
    }
    
    func removeSubcells(indexPath: IndexPath, deleteIPs: inout [IndexPath]) {
        for _ in deleteIPs {
            messages.remove(at: indexPath.row + 1)
        }
//        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: deleteIPs, with: .automatic)
//        self.tableView.endUpdates()
//        self.tableView.reloadData()
    }
    
    //MARK: - Action button operations
    @objc func showActionSheet(_ notification: NSNotification) {
        
        guard let dict = notification.userInfo as NSDictionary?,
            let textAnswer = dict["textAnswer"] as? String,
            let message = dict["message"] as? ChatEntry,
            let separatedUrl = message.buttonUrl.components(separatedBy: "?id=") as [String]?,
            let id = separatedUrl.last,
            let url = separatedUrl.first,
//            let isText = dict["isText"] as? Bool,
            let phone = Session.phone else {return}
        let taskType = message.taskType
        
        if taskType == "TEXT_IMAGE" || taskType == "IMAGE" {
            imageAction(taskType: taskType, phone: phone, textAnswer: textAnswer, id: id)
        } else {
            textAction(taskType: taskType, url: url, phone: phone, textAnswer: textAnswer, id: id)
        }
        
//        self.chatHistiry(withDelay: 5)
        
        print("***Task type = \(message.taskType)***\n***Text = \(message.text)***\n***Image = \(message.imageUrl!)")
    }
    
    func textAction(taskType:String, url:String, phone:String, textAnswer:String, id:String) {
        if textAnswer.count != 0 {
            Communicator.textAnswer(url: url, phone: phone, id: id, textAnswer: textAnswer) { [unowned self] _ in
                self.chatHistiry(withDelay: 1)
            }
        } else {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Введите ответ", style: .cancel, handler:{ (UIAlertAction)in
            }))
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
    }
    
    func imageAction(taskType:String, phone:String, textAnswer:String, id:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if (taskType == "TEXT_IMAGE" && textAnswer.count != 0) || taskType != "TEXT_IMAGE" {
            alert.addAction(UIAlertAction(title: "Сканировать серийный номер", style: .default , handler:{ (UIAlertAction)in
                self.getImage(fromSourceType: .camera) { image in
                    Communicator.photoAnswer(phone: phone, photo: image, id: id, textAnswer: textAnswer, completion: { [unowned self] _ in
                        self.chatHistiry(withDelay: 1) })
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Сканировать QR-код", style: .default , handler:{ (UIAlertAction)in
                self.getScanerResult({ text, _ in
                    Communicator.photoAnswer(phone: phone, id: id, textAnswer: text, completion: { [unowned self] _ in
                        self.chatHistiry(withDelay: 1)
                    })
                })
            }))
            
            alert.addAction(UIAlertAction(title: "Камера", style: .default , handler:{ (UIAlertAction)in
                self.getImage(fromSourceType: .camera) { image in
                    Communicator.photoAnswer(phone: phone, photo: image, id: id, textAnswer: textAnswer, completion: { [unowned self] _ in
                        self.chatHistiry(withDelay: 1)
                    })
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Галерея", style: .default , handler:{ (UIAlertAction)in
                self.getImage(fromSourceType: .photoLibrary) { image in
                    Communicator.photoAnswer(phone: phone, photo: image, id: id, textAnswer: textAnswer, completion: { [unowned self] _ in
                        self.chatHistiry(withDelay: 1) })
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Отмена button")
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Введите ответ", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Введите ответ button")
            }))
        }
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    private func getScanerResult(_ callback: @escaping ((String, String) -> ())) {
        getTextCallback = callback
        self.performSegue(withIdentifier: "scanCode", sender: nil)
    }
    
    //MARK: - ImagePickerController
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType, _ callback: @escaping ((UIImage) -> ())) {
        pickImageCallback = callback;
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if case let controller as BarcodeReaderViewController = segue.destination,
            segue.identifier == "scanCode" {
            controller.completion = { [unowned self] text, type in
                self.getTextCallback?(text, type)
            }
        }
    }

}

//MARK: - Table view
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.messages.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let isONE = messages[indexPath.row].isONE
        switch isONE {
        case true:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TextPlusImageTypeViewCell.self), for: indexPath) as! TextPlusImageTypeViewCell
            cell.actionBtnTitle.text = ""
            cell.textField.text = ""
            cell.questions = [String]()
            for view in cell.checkboxStackview.arrangedSubviews {
                view.removeFromSuperview()
            }
            if Session.indexPath != indexPath {
                cell.openCloseList.setImage(UIImage(named: "arrow_down"), for: .normal)
                cell.isShowSubcells = false
            } else {
                cell.openCloseList.setImage(UIImage(named: "arrow_up"), for: .normal)
                cell.isShowSubcells = true
            }
            cell.setupCell(message: self.messages[indexPath.row])
            return cell
        case false:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TextPlusImageSubCell.self), for: indexPath) as! TextPlusImageSubCell
            cell.setupCell(message: self.messages[indexPath.row])
            return cell
        }

    }

}

//MARK: - ImagePickerControllerDelegate
extension ChatViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        pickImageCallback?(image)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
    }
}
