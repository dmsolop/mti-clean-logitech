//
//  ProfileViewController.swift
//  Loyality
//
//  Created by Dima on 4/26/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var textFields = [UITextField]()
    var imagePicker: UIImagePickerController!
    
    private let patterns: [TextFieldType : String] = [.name : RegEx.name,
                                                      .mail : RegEx.mail]
    enum TextFieldType: Int {
        case name, mail
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFields = [nameTextField, emailTextField]
        nameTextField.text = Session.name
        emailTextField.text = Session.mail
        balanceLabel.text = "₴ \(Session.balance)"
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
        avatarImageView.layer.borderWidth = 3
        avatarImageView.layer.borderColor = #colorLiteral(red: 0.2788380682, green: 0.5658374429, blue: 0.8813762069, alpha: 1)
        avatarImageView.image = getSavedImage(named: "avatarImage.png") ?? UIImage(named: "avatar_bg")
        textFields.forEach { (textField) in
            textField.layer.borderWidth = 1
            textField.layer.borderColor = UIColor.simpleGray.cgColor
            textField.layer.cornerRadius = 8
            textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
     }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let isValid = isAllFieldsValid()
        
        saveButton.isUserInteractionEnabled = isValid
        saveButton.backgroundColor = (isValid) ? UIColor.lightBlue : UIColor.simpleGray
        saveButton.titleLabel?.textColor = (isValid) ? UIColor.white : UIColor.darkText

        if let type = TextFieldType(rawValue: textField.tag) {
            guard let pattern = patterns[type] else { return }
            let isVlaidText = textField.text?.checkToPattern(pattern: pattern) ?? false
            textField.layer.borderColor = (isVlaidText) ? UIColor.lightBlue.cgColor : UIColor.red.cgColor
        }
    }
    
    private func isAllFieldsValid() -> Bool {
        var isValid = true
        
        textFields.forEach { (textField) in
            if let type = TextFieldType(rawValue: textField.tag) {
                guard let pattern = patterns[type] else { return }
                if textField.text?.checkToPattern(pattern: pattern) ?? false && isValid {
                    isValid = true
                } else {
                    isValid = false
                }
            }
        }
        
        return isValid
    }
    
    @IBAction func changeAvatarClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Выберите фото", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Отменить", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveClicked(_ sender: UIButton) {
        Session.name = nameTextField.text
        Session.mail = emailTextField.text
        self.navigationController?.popViewController(animated: true)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Внимание", message: "Камера не доступна", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Внимание", message: "У Вас нет разрешения на доступ к галерее", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension ProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let avatarImage = info[.originalImage] as? UIImage
        avatarImageView.image = avatarImage
        let _ = saveImage(image: avatarImage!)
    }
    
    func saveImage(image: UIImage) -> Bool {
        guard let data = image.jpegData(compressionQuality:1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("avatarImage.png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
}

extension ProfileViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let count = textField.text?.count else { return false }
        return count <= 100
    }

    
}
