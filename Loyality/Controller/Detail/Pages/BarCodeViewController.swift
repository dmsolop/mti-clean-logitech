//
//  SendViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 4/7/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import TGLStackedViewController
import RSBarcodes_Swift
import AVFoundation

class BarCodeViewController: BasePageViewController {
    
    //MARK: - IBOutlets and properties
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet weak var code128: UIImageView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var textBarcode: UILabel!
    @IBOutlet weak var balanceView: UIView!
    
    @IBOutlet weak var addNewCardView: UIView!
    @IBOutlet weak var addNewCardTitle: UILabel!
    @IBOutlet weak var addNewCardText: UILabel!
    @IBOutlet weak var addNewCardTextField: UITextField!
    @IBOutlet weak var addNewCardBtn: UIButton!
    
    var partner: Partner!
    
    //MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        addNewCardTextField.delegate = self
        setUpBarCodeView()
    }
    
    private func setUpBarCodeView() {
        balanceLabel.text = "\(partner.balance)"
        balanceView.backgroundColor = partner.circleColor()
        addNewCardView.backgroundColor = partner.circleColor()
        addNewCardBtn.tintColor = partner.circleColor()
        
        if partner.isDiscrete && partner.barcodes.count > 1 {
            
        } else if partner.circleLogo.contains("fishka") && partner.active == 2 {
            
            containerView.isHidden = true
            addNewCardView.isHidden = false
            code128.isHidden = true
            balanceView.isHidden = true
            textBarcode.isHidden = true
            
        } else if let barcode = partner.barcodes.first {
            containerView.isHidden = true
            addNewCardView.isHidden = true
            code128.isHidden = false
            balanceView.isHidden = false
            textBarcode.isHidden = false
            
            let isValidEAN13 = RSUnifiedCodeValidator.shared.isValid(barcode.barCode, machineReadableCodeObjectType: AVMetadataObject.ObjectType.ean13.rawValue)
            
            textBarcode.text = barcode.barCode
            
            if isValidEAN13 {
                code128.image = RSUnifiedCodeGenerator.shared.generateCode(barcode.barCode, machineReadableCodeObjectType: AVMetadataObject.ObjectType.ean13.rawValue)
            } else {
                code128.image = RSUnifiedCodeGenerator.shared.generateCode(barcode.barCode, machineReadableCodeObjectType: AVMetadataObject.ObjectType.code128.rawValue)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCardsController" {
            (segue.destination as! CodeViewController).partner = partner
        }
    }

        //MARK: - Actions
    @IBAction func addNewCardDidPush(_ sender: UIButton) {
        sendCardRequest()
    }

    private func sendCardRequest() {
        if let text = addNewCardTextField.text, let phone = Session.phone {
            Communicator.setCard(phone: phone, cardNumber: text) { [unowned self] (result) in
                
                switch result["STATUS"] as! Int {
                case 0:
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: MainViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                case 1:
                    let notExistCardText = "Указанная карта не существует."
                    self.showAllertMessage(with: notExistCardText)
                case 2:
                    let errorAddingCardText = "Ошибка добавления карты."
                    self.showAllertMessage(with: errorAddingCardText)
                default:
                    return
                    
                }
            }
        }

    }
    
    func showAllertMessage(with text: String) {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

//MARK: - UITextFieldDelegate
extension BarCodeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text, text.count < 14 || string == "" {
            return true
        }
        return false
    }

}

