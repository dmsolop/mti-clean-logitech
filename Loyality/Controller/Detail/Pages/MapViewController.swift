//
//  MapViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 4/7/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class MapViewController: BasePageViewController {

    @IBOutlet private weak var mapView: GMSMapView!
    
    let locationManager = CLLocationManager()
    
    var latitude = 50.45466 {
        didSet {
            print("Latitude: \(latitude)")
        }
    }
    var longitude = 30.5238 {
        didSet {
            print("Longitude: \(longitude)")
        }
    }
    
    var pins = [MapObject]()
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {   switch status {
    case .restricted, .denied:
        // Disable your app's location features
//        disableMyLocationBasedFeatures()
    print(".restricted, .denied")
        break

    case .authorizedWhenInUse:

        print("authorizedWhenInUse")
        
        let camera = GMSCameraPosition.camera(withLatitude: manager.location?.coordinate.latitude ?? latitude, longitude: manager.location?.coordinate.longitude ?? longitude, zoom: 9.0)
        mapView.camera = camera

        break
        
    case .authorizedAlways:
        // Enable any of your app's location services.
//        enableMyAlwaysFeatures()
        print(".authorizedAlways")
    
         let camera = GMSCameraPosition.camera(withLatitude: manager.location?.coordinate.latitude ?? latitude, longitude: manager.location?.coordinate.longitude ?? longitude, zoom: 9.0)
        mapView.camera = camera

        break
        
    case .notDetermined:
        print(".notDetermined")
        manager.requestWhenInUseAuthorization()
        break
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        var locManager = CLLocationManager()
//        locManager.requestWhenInUseAuthorization()
        var currentLocation: CLLocation!
                if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() ==  .authorizedAlways){
        currentLocation = locManager.location
                    let camera = GMSCameraPosition.camera(withLatitude: locManager.location?.coordinate.latitude ?? latitude, longitude: locManager.location?.coordinate.longitude ?? longitude, zoom: 9.0)
        mapView.camera = camera
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        

    }
    
    func initialSetup() {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
   
        mapView.mapType = .normal
        mapView.settings.zoomGestures = true
        mapView.isMyLocationEnabled = true
        
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 9.0)
        mapView.camera = camera
        
        mapView.animate(to: camera)
//        mapView.
        
        
        pins.forEach { (pin) in
            let marker = GMSMarker(position: pin.coordinates())
//            marker.position = CLLocationCoordinate2D(latitude: pin.coordinates().latitude, longitude: pin.coordinates().longitude)
            marker.title = "Sydney"
//            marker.snippet = "Australia"
            
            marker.map = mapView
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.latitude = locValue.latitude
        self.longitude = locValue.longitude
    }
    
}
