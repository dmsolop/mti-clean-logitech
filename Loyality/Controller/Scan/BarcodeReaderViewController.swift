//
//  BarcodeReaderViewController.swift
//  Loyality
//
//  Created by Dima on 8/11/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import RSBarcodes_Swift
import AVFoundation

class BarcodeReaderViewController: RSCodeReaderViewController {
    
    //Mark: - Properties
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet var toggle: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var useResultBtn: UIButton!
    
    var completion: ((String, String) -> ())?
    
    var barcode: String = ""
    var barcodeType: String = ""
    var dispatched: Bool = false
    
    //MARK: - View lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: NOTE: Uncomment the following line to enable crazy mode
        // self.isCrazyMode = true
        
        self.resultLabel.isHidden = true
        self.useResultBtn.isHidden = true
        
        self.focusMarkLayer.strokeColor = UIColor.red.cgColor
        
        self.cornersLayer.strokeColor = UIColor.yellow.cgColor
        
        self.tapHandler = { point in
            print(point)
        }
        
        // MARK: NOTE: If you want to detect specific barcode types, you should update the types
        var types = self.output.availableMetadataObjectTypes
        // MARK: NOTE: Uncomment the following line remove QRCode scanning capability
        // types = types.filter({ $0 != AVMetadataObject.ObjectType.qr })
        self.output.metadataObjectTypes = types
        
        // MARK: NOTE: If you layout views in storyboard, you should these 3 lines
        for subview in self.view.subviews {
            self.view.bringSubviewToFront(subview)
        }
        
        self.toggle.isEnabled = self.hasTorch()
        
        self.barcodesHandler = { barcodes in
            if !self.dispatched { // triggers for only once
                self.dispatched = true
                for barcode in barcodes {
                    guard let barcodeString = barcode.stringValue else { continue }
                    self.barcodeType = barcode.type.rawValue
                    self.barcode = barcodeString
                    print("Barcode found: type=" + barcode.type.rawValue + " value=" + barcodeString, "\nBarcode all = \(barcode)")
                    
                    DispatchQueue.main.async(execute: {
//                        self.performSegue(withIdentifier: "nextView", sender: self)
                        
                        self.resultLabel.isHidden = false
                        self.useResultBtn.isHidden = false
                        
                        self.resultLabel.text = /*barcodeType + " = " +*/ barcodeString
                        
                    })
                    
                    // MARK: NOTE: break here to only handle the first barcode object
                    break
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.dispatched = false // reset the flag so user can do another scan
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - Actions
    @IBAction func close(_ sender: AnyObject?) {
        self.navigationController?.popViewController(animated: true)
        print("close called.")
    }
    
    @IBAction func toggle(_ sender: AnyObject?) {
        let isTorchOn = self.toggleTorch()
        print(isTorchOn)
    }
    
    
    @IBAction func UseResultDidPush(_ sender: UIButton) {
        self.getDataAndBack()
    }
    
    private func getDataAndBack() {
        guard let text = resultLabel.text, text != "" else {
            completion?("No data", "No type")
            self.navigationController?.popViewController(animated: true)
            return
        }
        completion?(text, barcodeType)
        self.navigationController?.popViewController(animated: true)
    }
    
}
