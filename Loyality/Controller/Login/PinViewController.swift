//
//  PinViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 3/20/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import SVProgressHUD

class PinViewController: BaseViewController {

    @IBOutlet weak var pinTextField: JVFloatLabeledTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pinTextField.delegate = self
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if Session.pin == pinTextField.text {
            Session.isCheckin = false
            showLoader()
            Communicator.login(phone: Session.phone!) { [weak self] (response) in
                Threads.performTaskInMainQueue {
                    self?.hideLoader()
                    self?.perform(segue: StoryboardSegue.Main.showMainController)
                }
            }
        }
    }
    
    @IBAction func registrationButtonPressed(_ sender: UIButton) {
        perform(segue: StoryboardSegue.Main.showRegistrationController)
    }
    
}

extension PinViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 4
    }
}
