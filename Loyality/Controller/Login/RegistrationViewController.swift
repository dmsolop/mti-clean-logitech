
//
//  RegistrationViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 2/18/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class RegistrationViewController: BaseViewController {
    @IBOutlet weak var nameTextField: JVFloatLabeledTextField!
    @IBOutlet weak var phoneTextField: JVFloatLabeledTextField!
    @IBOutlet weak var mailTextField: JVFloatLabeledTextField!
    @IBOutlet weak var pinTextField: JVFloatLabeledTextField!
    @IBOutlet var textFields: [JVFloatLabeledTextField]!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var imagePicker: UIImagePickerController!
    
    private var phoneStore = ""
    private var pinStore: String?
    
    private let patterns: [TextFieldType : String] = [.name : RegEx.name,
                                                      .mail : RegEx.mail,
                                                      .phone : RegEx.phone,
                                                      .pin : RegEx.pin]
    enum TextFieldType: Int {
        case name, phone, mail, pin, none
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        textFieldDidChange()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case StoryboardSegue.Main.showVerificationController.rawValue:
            (segue.destination as! VerificationViewController).pin = self.pinStore!
        default:
            break
        }
    }
    
    func setupStyle() {
        textFields.forEach { (textField) in
            textField.layer.borderWidth = 1
            textField.layer.borderColor = UIColor.simpleGray.cgColor
            textField.layer.cornerRadius = 8
            textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
        avatarButton.layer.cornerRadius = avatarButton.frame.height / 2
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
        avatarImageView.image = getSavedImage(named: "avatarImage.png") ?? UIImage(named: "avatar_bg")
    }
    
    @objc func textFieldDidChange(_ textField: UITextField = UITextField()) {
        let isValid = isAllFieldsValid()
        
        registrationButton.isUserInteractionEnabled = isValid
        registrationButton.backgroundColor = (isValid) ? UIColor.lightBlue : UIColor.simpleGray
        registrationButton.titleLabel?.textColor = (isValid) ? UIColor.white : UIColor.darkText
        
        if let type = TextFieldType(rawValue: textField.tag) {
            guard let pattern = patterns[type] else { return }
            let isVlaidText = textField.text?.checkToPattern(pattern: pattern) ?? false
            textField.layer.borderColor = (isVlaidText) ? UIColor.lightBlue.cgColor : UIColor.red.cgColor
        }
    }
    
    @IBAction func getAvatarImage(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Выберите фото", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Отменить", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)

    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Внимание", message: "Камера не доступна", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Внимание", message: "У Вас нет разрешения на доступ к галерее", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        registrationButton.isUserInteractionEnabled = false
        Session.name = nameTextField.text
        Session.phone = phoneTextField.text
        Session.mail = mailTextField.text
        self.pinStore = pinTextField.text
        
        guard let phone = phoneTextField.text else {
            return
        }

        Communicator.registration(phone: phone) { [weak self] () in
            Threads.performTaskInMainQueue {
                self?.perform(segue: StoryboardSegue.Main.showVerificationController, sender: nil)
            }
        }
    }
    

    
    private func isAllFieldsValid() -> Bool {
        var isValid = true
        
        textFields.forEach { (textField) in
            if let type = TextFieldType(rawValue: textField.tag) {
                guard let pattern = patterns[type] else { return }
                if textField.text?.checkToPattern(pattern: pattern) ?? false && isValid {
                    isValid = true
                } else {
                    isValid = false
                }
            }
        }
        
        return isValid
    }
}

extension RegistrationViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let avatarImage = info[.originalImage] as? UIImage
        avatarImageView.image = avatarImage
        let success = saveImage(image: avatarImage!)
    }
    
    func saveImage(image: UIImage) -> Bool {
        guard let data = image.jpegData(compressionQuality:1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("avatarImage.png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
}

extension RegistrationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.lightBlue.cgColor
        
        if let type = TextFieldType(rawValue: textField.tag), type == .phone {
            textField.text = self.phoneStore.count != 0 ? self.phoneStore : "380"
        }
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.layer.borderColor = UIColor.simpleGray.cgColor
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let type = TextFieldType(rawValue: textField.tag) {
            switch type {
            case .name:
                textField.resignFirstResponder()
                textFields?[TextFieldType.phone.rawValue].becomeFirstResponder()
                return true
            case .phone:
                textField.resignFirstResponder()
                textFields?[TextFieldType.mail.rawValue].becomeFirstResponder()
                return true
            case .mail:
                textField.resignFirstResponder()
                textFields?[TextFieldType.pin.rawValue].becomeFirstResponder()
                return true
            case .pin:
                textField.resignFirstResponder()
                return true
            default:
                return false
            }
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let type = TextFieldType(rawValue: textField.tag), type == .pin {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 4
        } else if let type = TextFieldType(rawValue: textField.tag), type == .phone {
            self.phoneStore = textField.text! + string
            guard let count = textField.text?.count else { return false }
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                
                if isBackSpace == -92, count == 12 {
                    var tempPhone = self.phoneStore
                    tempPhone.removeLast()
                    textField.text = tempPhone
                    self.textFieldDidChange(textField)
                }
            }
            return count <= 11
        } else {
            guard let count = textField.text?.count else { return false }
            return count <= 100
        }
    }
    
}
