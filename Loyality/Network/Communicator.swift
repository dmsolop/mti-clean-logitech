//
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//


import UIKit
import Alamofire
import ObjectMapper

typealias SuccessHandler = ((Bool) -> Void)?
typealias EmptyBlock = (() -> ())
typealias ResponseBlock = (([String: Any]) -> Void)


class Communicator: NSObject {

    typealias block = ((Any) -> Void)?

    private class func sendRequest(request: String, method: Alamofire.HTTPMethod, parameters: Parameters? = nil, headers: Bool = false, completion: @escaping([String: Any]) -> Void) {
        Requester.sendRequest(request: request, method: method, parameters: parameters, headers: headers) { response in
            completion(response)
            print(response)
        }
    }
    
    private class func sendUpload(request: String, parameters: Parameters? = nil, image: Data?, completion: @escaping(DataResponse<String>) -> Void) {
        Requester.sendUpload(upload: request, parameters: parameters, image: image) { response in
            completion(response)
            print(response)
        }
    }

    
    //    MARK: - Login flow
    class func registration(phone: String, completion: @escaping EmptyBlock) {
        let request = APIConfigs.request(part: "SecretKeyRequest")
        let parameters: [String: Any] = ["TYPE": "reg_req_1",
                                         "COMPANY_ID" : 8,
                                         "ID": phone]
        
        sendRequest(request: request, method: .post, parameters: parameters, headers: true) { response in
            completion()
        }
    }
    
    class func registration(phone: String, sms: String, name: String, email: String, completion: @escaping ResponseBlock) {
        guard let pk = Security.publicKey() else { return }
        let sucreString = "reg_req_2" + phone + pk /*Security.publicKey()!*/
        let signature = Security.encryptAES(message: sucreString.md5().uppercased(), key: sms) //F6DTIT
        
        
        let request = APIConfigs.request(part: "UserRegistration")
        let parameters: [String: Any] = ["TYPE": "reg_req_2",
                                         "ID": phone,
                                         "PK": pk,
                                         "NAME": name,
                                         "EMAIL": email,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            completion(response)
        }
    }
    
    class func login(phone: String, completion: @escaping ResponseBlock) {
        let sucureString = "reg_balance"+phone
        let signature = Security.generateSignature(message: sucureString)
        
        let request = APIConfigs.request(part: "Login")
        let parameters: [String: Any] = ["TYPE": "reg_balance",
                                         "USER_ID": phone,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            Session.balance = response["BALANCE"] as? Int ?? 0
            completion(response)
        }
    }
    
    class func partner(phone: String, completion: @escaping (([Partner]) -> Void)) {
        let sucureString = "reg_partner"+phone
        let signature = Security.generateSignature(message: sucureString)
        
        let request = APIConfigs.request(part: "request_partners")
        let parameters: [String: Any] = ["TYPE": "reg_partner",
                                         "USER_ID": phone,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            var partners = [Partner]()
            let items = response["PARTNERS"] as? [[String: Any]] ?? []
            
            items.forEach({ (item) in
                if let partner = Mapper<Partner>().map(JSON: item) {
                    partners.append(partner)
                }
            })
            
            completion(partners)
        }
    }
    
    
    class func payment(phone: String, partner: Int, amount: Int, phoneTopUp: String? = nil, completion: @escaping ResponseBlock) {
        let sucureString = "reg_payment"+phone
        let signature = Security.generateSignature(message: sucureString)
        
        let request = APIConfigs.request(part: "request_payment")
        var parameters: [String: Any] = ["TYPE": "reg_payment",
                                         "USER_ID": phone,
                                         "PARTNER_ID": partner,
                                         "AMOUNT" : amount,
                                         "SIGNATURE": signature]
        
        if let userPhone = phoneTopUp {
            parameters["PHONE_TOPUP"] = userPhone //Номер телефона
        }
        
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            completion(response)
        }
    }
    
    class func map(phone: String, completion: @escaping (([MapObject]) -> Void)) {
        let sucureString = "reg_map"+phone
        let signature = Security.generateSignature(message: sucureString)
        
        let request = APIConfigs.request(part: "request_map")
        let parameters: [String: Any] = ["TYPE": "reg_map",
                                         "USER_ID": phone,
                                         "DB_VERSION": 0,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            var maps = [MapObject]()
            let items = response["MAPS"] as? [[String: Any]] ?? []
            
            items.forEach({ (item) in
                if let map = Mapper<MapObject>().map(JSON: item) {
                    maps.append(map)
                }
            })
            
            completion(maps)
        }
    }
    
    class func sendFcmKey(phone: String, fcmKey: String, completion: @escaping EmptyBlock) {
        let sucureString = "messenger_reg"+phone
        let signature = Security.generateSignature(message: sucureString)
        
        let request = APIConfigs.request(part: "messenger_reg")
        let parameters: [String: Any] = ["TYPE": "reg_messenger",
                                         "REGISTRATION_ID": fcmKey,
                                         "USER_ID": phone,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters, headers: true) { response in
            completion()
        }
    }
    
    class func checkIn(phone: String, qrCode: String, completion: @escaping ResponseBlock) {
        let sucreString = "checkin" + phone
        let signature = Security.generateSignature(message: sucreString)
        
        let request = APIConfigs.request(part: "checkin")
        let parameters: [String: Any] = ["TYPE": "checkin",
                                         "USER_ID": phone,
                                         "TP_ID": qrCode,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            completion(response)
        }
    }
    
    class func setCard(phone: String, cardNumber: String, completion: @escaping ResponseBlock) {
        let sucreString = "m_card_req" + phone
        let signature = Security.generateSignature(message: sucreString)
        
        let request = APIConfigs.request(part: "set_card")
        let parameters: [String: Any] = ["TYPE": "m_card_req",
                                         "USER_ID": phone,
                                         "fishkaCard": cardNumber,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            completion(response)
        }
    }
    
    class func textAnswer(url: String, phone: String, id: String, textAnswer: String, tpId:String = "", tpY:String = "", tpX:String = "", completion: @escaping ResponseBlock) {
        let csCopy = CharacterSet(bitmapRepresentation: CharacterSet.urlPathAllowed.bitmapRepresentation)
        let encodedAnswer = textAnswer.addingPercentEncoding(withAllowedCharacters: csCopy)!
        let parameters: [String: Any] = ["id": id,
                                         "value": encodedAnswer,
                                         "user_id": phone,
                                         "tp_id": tpId,
                                         "tp_y": tpY,
                                         "tp_x": tpX]
        
        sendRequest(request: url, method: .get, parameters: parameters, headers: true) { response in
            DispatchQueue.main.async {
                completion(response)
            }
        }
    }
    
    class func photoAnswer(phone: String, photo: UIImage? = nil, id: String, textAnswer: String = "", tpId: String = "", tpY: String = "", tpX: String = "", completion: @escaping (DataResponse<String>) -> Void) {
        let imageData:Data = photo?.jpegData(compressionQuality: 0.4) ?? Data()
        let parameters: [String: Any] = ["id": id,
                                         "txt": textAnswer,
                                         "user_id": phone,
                                         "tp_id": tpId,
                                         "tp_y": tpY,
                                         "tp_x": tpX]
        let request = APIConfigs.request(part: "task_messenger_image")
        sendUpload(request: request, parameters: parameters, image: imageData) { response in
            DispatchQueue.main.async {
                completion(response)
            }
        }

    }
    
    class func chatHistory(phone: String, completion: @escaping (([ChatEntry]) -> Void)) {
        let sucureString = "1000"+"message_history"+phone
        let signature = Security.generateSignature(message: sucureString)
        
        let request = APIConfigs.request(part: "messenger_history")
        
        let df = DateFormatter()
        
        df.dateFormat = "MMM d, yyyy hh:mm:ss a"
        //        "yyyy-MM-dd hh:mm:ss"
        let now = df.string(from: Date())
        let earlyDate = Calendar.current.date(
            byAdding: .year,
            value: -1,
            to: Date())
        let satr = df.string(from: earlyDate!)
        print(now)
        print(satr)
        
        let parameters: [String: Any] = ["LIMIT": 1000,
                                         "TYPE":"message_history",
                                         "FINISH": now,
                                         "START": satr,
                                         "USER_ID": phone,
                                         "SIGNATURE": signature]
        
        sendRequest(request: request, method: .post, parameters: parameters) { response in
            DispatchQueue.main.async {
                var list = [ChatEntry]()
                guard let status = response["MESSAGES"]    else {
                    return
                }
                
                for element in status as! NSArray{
                    
                    let el = element as! NSDictionary
                    let entry = ChatEntry()
                    entry.avatar = el["AVATAR"] as? String
                    entry.buttonText = el["BUTTON_CAPTION"] as? String
                    entry.buttonUrl = el["BUTTON_URL"] as? String ?? ""
                    entry.dateAndTime = el["DATE_TIME"] as? String ?? ""
                    if let date = df.date(from: entry.dateAndTime) {
                        entry.date = date
                    } else {
                        entry.date = Date()
                    }
                    
                    entry.fromUserId = el["FROM_USER_ID"] as! String
                    if let imgUrlStr = el["IMAGE"] as? String {
                        let string = String(utf8String: imgUrlStr.cString(using: String.Encoding.utf8)!)
                        entry.imageUrl = string
                    }
                    //                entry.imageUrl = el["IMAGE"] as? String
                    guard let t = el["TASK"] else {
                        list.append(entry)
                        continue
                    }
                    
                    let task = t as! NSDictionary
                    entry.answer = task["ANSWER"] as? String ?? ""
                    entry.answer2 = task["ANSWER2"] as? String ?? ""
                    entry.taskCost = task["TASK_COST"] as? Int ?? 0
                    entry.taskCoastType = task["TASK_COST_TYPE"] as? Int ?? 0
                    entry.taskId = task["TASK_ID"] as? Int ?? 0
                    entry.taskType = task["TASK_TYPE"] as? String ?? ""
                    entry.status = task["STATUS"] as? Int ?? 0
                    entry.text = el["TEXT"] as! String
                    entry.toUserId = el["TO_USER_ID"] as! String
                    
                    if let q  = task["QUESTIONS"] {
                        var res = ""
                        for qw in q as! NSArray{
                            let   qwes = qw as! NSDictionary
                            if  qwes["TEXT"] != nil, (qwes["TEXT"] as! String) != "" {
                                res = res + (qwes["TEXT"] as! String) + "###"
                            }
                        }
                        if res != "" {
                            entry.questions = res
                        }
                    }
                    list.append(entry)
                }
                
                var set = Set<Int>()
                var firstLists = [ChatEntry]()
                
                for z in list {
                    if set.contains(z.taskId) {
                        z.isONE = false
                    } else {
                        z.isONE = true
                        set.insert(z.taskId)
                        firstLists.append(z)
                    }
                }
                
                for z in firstLists {
                    for zz in list {
                        if zz.taskId == z.taskId && !zz.isONE {
                            z.list.append(zz)
                        }
                    }
                }
                
                completion(firstLists)
            }

        }
    }
}
