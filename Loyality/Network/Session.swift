//
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//


import UIKit

enum SessionKey: String {
    case accessToken = "ACCESS_TOKEN"
    case name = "USER_NAME"
    case phone = "USER_PHONE"
    case mail = "USER_MAIL"
    case logs = "LOGS"
    case pin = "PIN"
    case signature = "SIGNATURE"
    case balance = "BALANCE"
    case indexPaths = "INDEX_PATHS"
    case indexPath = "INDEX_PATH"
    case fcmKey = "FCM_KEY"
    case isCheckin = "IS_CHECKIN"
    
    static let allValues: [SessionKey] = [accessToken, name, phone, mail, logs, pin, signature, balance, indexPaths, indexPath, fcmKey, isCheckin]

}

// TODO: create object instead

class Session: NSObject {

    static var logs: [String] {
        get {
            return sessionValue(get: .logs) as? [String] ?? []
        }
        set {
            sessionValue(set: newValue, key: .logs)
        }
    }
    
    static var isCheckin: Bool {
        get {
            return sessionValue(get: .isCheckin) as? Bool ?? false
        }
        set {
            sessionValue(set: newValue, key: .isCheckin)
        }
    }

    static var accessToken: String? {
        get {
            return sessionValue(get: .accessToken) as? String
        }
        set {
            sessionValue(set: newValue, key: .accessToken)
        }
    }
    
    static var pin: String? {
        get {
            return sessionValue(get: .pin) as? String
        }
        set {
            sessionValue(set: newValue, key: .pin)
        }
    }
    
    static var signature: String? {
        get {
            return sessionValue(get: .signature) as? String
        }
        set {
            sessionValue(set: newValue, key: .signature)
        }
    }

    static var name: String? {
        get {
            return sessionValue(get: .name) as? String
        }
        set {
            sessionValue(set: newValue, key: .name)
        }
    }

    static var phone: String? {
        get {
            return sessionValue(get: .phone) as? String
        }
        set {
            sessionValue(set: newValue, key: .phone)
        }
    }

    static var mail: String? {
        get {
            return sessionValue(get: .mail) as? String
        }
        set {
            sessionValue(set: newValue, key: .mail)
        }
    }

//    static var password: String? {
//        get {
//            return sessionValue(get: .password) as? String
//        }
//        set {
//            sessionValue(set: newValue, key: .password)
//        }
//    }
//
    static var balance: Int {
        get {
            return sessionValue(get: .balance) as? Int ?? 0
        }
        set {
            sessionValue(set: newValue, key: .balance)
        }
    }
    
    static var listIndexPaths: [IndexPath] {
        get {
            if let data = sessionValue(get: .indexPaths) as? Data {
                return NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! [IndexPath]
            }
            return []
        }
        set {
            let data = NSKeyedArchiver.archivedData(withRootObject: newValue)
            sessionValue(set: data, key: .indexPaths)
        }
    }
    
    static var indexPath: IndexPath? {
        get {
            if let data = sessionValue(get: .indexPath) as? Data {
                return NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? IndexPath
            }
            return nil
        }
        set {
            let data = NSKeyedArchiver.archivedData(withRootObject: newValue as Any)
            sessionValue(set: data, key: .indexPath)
        }
    }
    

    static var fcmKey: String? {
        get {
            return sessionValue(get: .fcmKey) as? String
        }
        set {
            sessionValue(set: newValue, key: .fcmKey)
        }
    }

    private static func sessionValue(get key: SessionKey) -> Any? {
        sessionKey = key.rawValue
        return sessionValue
    }

    private static func sessionValue(set value: Any?, key: SessionKey) {
        sessionKey = key.rawValue
        sessionValue = value
    }

    private static var sessionKey: String {
        get {
            return (UserDefaults.standard.object(forKey: "key") as? String ?? "")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "key")
        }
    }

    private static var sessionValue: Any? {
        get {
            return UserDefaults.standard.object(forKey: sessionKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: sessionKey)
        }
    }

    static var tintColor: UIColor {
        return UIColor(red: 13 / 255.0, green: 159 / 255.0, blue: 180 / 255.0, alpha: 1)
    }

    static var appRedColor: UIColor {
        return UIColor(red: 238 / 255, green: 92 / 255.0, blue: 92 / 255.0, alpha: 1)
    }
    
    static let triggerIdentifire = "notificationTriggerIdentifire"
    
    static let serverPhone = "380500000000"

}
