//
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//


import UIKit
import Alamofire

class ResponseValidator: NSObject {

    static let errorMessage = "Incorrect request"
    
    class func checkResponse(response : DataResponse<Any>, completion: @escaping (_ response : [String : Any]) -> Void) {
        
        let resultValue = String(data: response.data!, encoding: String.Encoding.utf8) ?? errorMessage
        
        
        if resultValue == errorMessage {
            manageTimeout(response: response)
            return
        } else {
            let result = response.result.value as? [String : AnyObject] ?? [:]
            completion(result)
        }
        
        
        
//        guard let dictionary = response.result.value as? [String : AnyObject]
//            else {
//                response.result.value as? [[String : AnyObject]] != nil ? completion(["response" : response.result.value!]) : completion([:])
//                return
//        }
//        if dictionary["errors"] != nil {
//            let errors = dictionary["errors"] as? [String: Any]
//            checkCodes(with: errors)
//        } else {
//            completion(dictionary)
//        }
    }

    private class func checkCodes(with code: [String: Any]?) {
        let title: String = code?.keys.first ?? "Error"
        let message = code?[title] as? [String]
        ServerError.show(alert: message?.first ?? "Something went wrong", title, "Ok", nil)
        
    }
    
    private static var excludedStatus : [String] {
        return ["success", "User have not hash", "updated"]
    }
    
    private class func manageTimeout(response : DataResponse<Any>) {
        if let error = response.result.error {
            if error._code == 4 {
                //JSON serialization error, ignoring
                return
            } else if error._code == NSURLErrorTimedOut || error._code == NSURLErrorCannotFindHost || error._code != -999 || error._code != -1008 {
                ServerError.showError(777)
            }
        }
    }
    
}
