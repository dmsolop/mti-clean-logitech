//
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//

import UIKit

struct Constants {
    
    static let appID = Bundle.main.bundleIdentifier!
}

public struct APIConfigs {

    static var timeoutInterval: Double {
        return 30
    }

    public var userPhone: String {
        return Session.phone?.replacingOccurrences(of: "+", with: "") ?? ""
    }

    public var uuid: String {
        return UIDevice.current.identifierForVendor!.uuidString
    }

    public var accessToken: String {
        return Session.accessToken ?? ""
    }

//    public var deviceToken: String {
//        return Session.deviceToken ?? ""
//    }
//
//    public var userID: Int {
//        return Session.id
//    }

    static var apiPrefix: String {
        return "http://78.111.215.82/LoyalityMTI/"
    }

    static var apiKey: String {
        return "AIzaSyD_B8-lW_OTZLZ6bW5MGa_E8aqfNLLIPF4"
    }
    
    static var apiDateFormat: String {
        return "yyyy-MM-dd'T'HH:mm:ssxxxxx"
    }
    
    static var apiDateFormatter : DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = apiDateFormat
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }
    
    static var header: [String: String] {
//        return ["Authorization": Session.accessToken ?? ""],
        return ["Content-Type": "text/json"]
    }
    
    static func request(part: String) -> String {
        return "\(apiPrefix)\(part)"
    }
}
