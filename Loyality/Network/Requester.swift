//
//  Requester.swift
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//

import UIKit
import Alamofire
import SystemConfiguration

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
    
}


class Requester: NSObject {
    
    static let shared = Requester()
    var sessionManager = Alamofire.SessionManager()
    private var reachabilityManager: NetworkReachabilityManager? = nil
    
    override init() {
        super.init()
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIConfigs.timeoutInterval
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    private static var manager: Alamofire.SessionManager {
        return shared.sessionManager
    }
    
    class func sendRequest(request requestPath: String, method: Alamofire.HTTPMethod, parameters: Parameters? = nil, headers: Bool = false, completion: @escaping([String: Any]) -> Void) {
        let encoding: ParameterEncoding = URLEncoding.httpBody
        let checkHeaders = APIConfigs.header //headers ? APIConfigs.header : nil
        
        var request = URLRequest(url: URL(string: requestPath)!)
        request.httpMethod = method.rawValue
        request.setValue("text/json", forHTTPHeaderField: "Content-Type")
        let pjson = parameters?.json
        let data = (pjson?.data(using: .utf8))! as Data
        
        request.httpBody = data
        
        if method == .get {
            
            manager.request(requestPath, method: method, parameters: parameters).responseString { (response) in
                
                DispatchQueue.main.async {
                    print(response)
                    switch response.result {
                        
                    case .success(let string):
                        print(string)

                        let result: [String: Any] = ["RESPONSE":response]
                        completion(result)
                    case .failure(let error):
                        print(error)
                    }
                }
            }
            
        } else {
            manager.request(request).responseJSON { (response) in
                
                DispatchQueue.main.async {
                    ResponseValidator.checkResponse(response: response, completion: { response in
                        completion(response)
                    })
                }
                print(response)
            }
        }
    }
    
    class func sendUpload(upload uploadPath: String, parameters: Parameters? = nil, image: Data? = nil, completion: @escaping(DataResponse<String>) -> Void) {
        guard let parameters = parameters else {
            print("this upload request has no parameters")
            return
        }
        
        manager.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            if let imageData = image {
                multipartFormData.append(imageData, withName: "upload", fileName: "attachment_file.jpg", mimeType: "image/*")
            }
        },
                       to: uploadPath,
                       encodingCompletion: { (result) in
                        switch result {
                        case .success(let upload, _, _):
                            
                            upload.uploadProgress(closure: { (progress) in
                                print("Upload Progress: \(progress.fractionCompleted)")
                            })
                            
                            upload.responseString(completionHandler: { (response) in
                                print(response.result)
                                completion(response)
                            })
                            
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                        
        })

    }
    
}
