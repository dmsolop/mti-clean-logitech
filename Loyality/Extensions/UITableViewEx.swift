//
//  UITableViewEx.swift
//  Loyality
//
//  Created by Dima on 9/2/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func scrollToCurrent(_ indexPath: IndexPath){
        DispatchQueue.main.async {
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}
