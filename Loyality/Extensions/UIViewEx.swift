//
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//

import UIKit
import AudioToolbox

extension UIView {

    private var defaultY: CGFloat {
        get {
            let barHeight = UIApplication.topViewController()?.navigationController?.navigationBar.frame.height ?? 0
            let statusBarHeight = UIApplication.shared.isStatusBarHidden ? CGFloat(0) : UIApplication.shared.statusBarFrame.height
            return barHeight + statusBarHeight
        }
    }

    func bindToKeyboard(constant: NSLayoutConstraint? = nil) {
        NotificationCenter.default.addObserver(self, selector: #selector(UIView.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UIView.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: constant)
    }

    func unbindToKeyboard(){
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: Notification) {
        let size = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        let keyboardY = UIScreen.main.bounds.size.height - size.height
        let viewY = frame.size.height + frame.origin.y + defaultY
        if keyboardY < viewY {
            let deltaY = viewY - keyboardY
            UIView.animate(withDuration: 0.3) {
                self.superview?.frame.origin.y = -deltaY
            }
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.3) {
            self.superview?.frame.origin.y = self.defaultY
        }
        guard let constraint = notification.object as? NSLayoutConstraint
            else {
                return
        }
        constraint.constant = 0
    }
    
    func fadeIn(duration: TimeInterval = 0.33, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.33, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }


    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }

    func addHole(rect: CGRect) {
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        let path = UIBezierPath(rect: bounds)
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        path.append(UIBezierPath(rect: rect))
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }

    func addGradient(colors: [CGColor], horizontal: Bool) {
        layer.sublayers?.forEach({ if $0 is CAGradientLayer { $0.removeFromSuperlayer() } })
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = colors
        if horizontal == true {
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        layer.insertSublayer(gradient, at: 0)
    }
    
    func showIndicator(color: UIColor) {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: (frame.size.width - 27) / 2, y: (frame.size.height - 27) / 2, width: 27, height: 27))
        activityIndicator.color = color
        activityIndicator.startAnimating()
        addSubview(activityIndicator)
        isUserInteractionEnabled = false
    }
    
    func hideIndicator() {
        isUserInteractionEnabled = true
        for subview in subviews {
            if subview is UIActivityIndicatorView {
                subview.removeFromSuperview()
                break
            }
        }
    }
    
    // TODO :- create selection for each corner
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
    }

    @IBInspectable var shadowOffset: CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }

    @IBInspectable var shadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }

    @IBInspectable var shadowRadius: CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }

    @IBInspectable var shadowColor: UIColor? {
        set {
            layer.shadowColor = newValue?.cgColor
        }
        get {
            return UIColor(cgColor: layer.shadowColor!)
        }
    }

    func addSameSizeConstraintSubview(_ subview: UIView) {
        addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([leftAnchor.constraint(equalTo: subview.leftAnchor),
                                     rightAnchor.constraint(equalTo: subview.rightAnchor),
                                     topAnchor.constraint(equalTo: subview.topAnchor),
                                     bottomAnchor.constraint(equalTo: subview.bottomAnchor)])
    }
    
    func addConstraintsWithFormatString(formate: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formate, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
        
    }
}
