//
//  UIImageEx.swift
//  Loyality
//
//  Created by Dima on 8/9/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

extension UIImage {
    
    func getCropRatio() -> CGFloat {
        let widthRatio = CGFloat(self.size.width / self.size.height)
        return widthRatio
    }
}
