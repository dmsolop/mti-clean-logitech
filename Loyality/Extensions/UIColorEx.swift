//
//  UIColorEx.swift
//  Loyality
//
//  Created by Denis Romashov on 3/19/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

extension UIColor {
    static let lightBlue = #colorLiteral(red: 0, green: 0.5198951364, blue: 0.8650086522, alpha: 1)
    static let simpleGray = #colorLiteral(red: 0.6705358028, green: 0.670617938, blue: 0.6705077291, alpha: 1)
//    static let textFieldShadow = #colorLiteral(red: 0.4224057496, green: 0.3398813903, blue: 0.5677258372, alpha: 1)
//    static let placeholder = #colorLiteral(red: 0.7607843137, green: 0.8, blue: 0.8588235294, alpha: 1)
//    static let textDark = #colorLiteral(red: 0.1490196078, green: 0.1490196078, blue: 0.1490196078, alpha: 1)
//    static let navigationTitle = #colorLiteral(red: 0.2944285572, green: 0.3378308713, blue: 0.4081071615, alpha: 1)
//    static let textSection = #colorLiteral(red: 0.5977378488, green: 0.6258977056, blue: 0.6955066323, alpha: 1)
//    static let cellSelectionBackground = #colorLiteral(red: 0.9607843137, green: 0.9647058824, blue: 0.9764705882, alpha: 1)
//    static let pink = #colorLiteral(red: 1, green: 0.374507308, blue: 0.5391514301, alpha: 1)

    public  convenience init(hex : String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            self.init(red: 1, green: 1, blue: 1, alpha: 1)
            return
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}


