//
//  UIResponderEx.swift
//  Loyality
//
//  Created by Dima on 8/6/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

extension UIResponder {
    
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
}
