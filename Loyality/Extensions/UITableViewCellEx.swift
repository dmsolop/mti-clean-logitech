//
//  UITableViewCellEx.swift
//  Loyality
//
//  Created by Dima on 8/6/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    var tableView: UITableView? {
        return next(UITableView.self)
    }
    
    var indexPath: IndexPath? {
        return tableView?.indexPath(for: self)
    }
}
