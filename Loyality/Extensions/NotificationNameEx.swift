//
//  NotificationNameEx.swift
//  Loyality
//
//  Created by Dima on 8/6/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let showHideSubcells = Notification.Name(rawValue: "showHideSubcells")
    static let showActionSheet = Notification.Name(rawValue: "showActionSheet")
    static let fcmToken = Notification.Name(rawValue: "FCMToken")
}
