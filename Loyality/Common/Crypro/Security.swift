//
//  Encryptor.swift
//  Loyality
//
//  Created by Denis Romashov on 1/5/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import CommonCrypto
import RSASwiftGenerator
//import SwiftyRSA
//import CryptoSwift
//import CryptorRSA

class Security {
    
//    static let publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDM1h0hXHS57IBM/iRP1/IsAkAu" +
//    "F/13wArLEOjD9XahaDiW+WVFiLoTn2vVO+xs5XfY9Fpmw05vVKBunmNomo/3zIuy" +
//    "h81hCM3u0YlznWn4WVb/yv4L7EeAZz2C1Qi3az3Zoc/PaKZfE2JdTF+3L7EtI5z9" +
//    "6C9zjqiuv+P2d7ct1wIDAQAB"
    
    static func publicKey() -> String? {
        let localHeimdall = Heimdall(tagPrefix: "com.development.mti")
        if let heimdall = localHeimdall, let publicKeyData = heimdall.publicKeyDataX509() {
            
            let publicKeyString = publicKeyData.base64EncodedString()
            // If you want to make this string URL safe,
            // you have to remember to do the reverse on the other side later
            //            publicKeyString = publicKeyString.replacingOccurrences(of: "/", with: "_")
            //            publicKeyString = publicKeyString.replacingOccurrences(of: "+", with: "-")
            
            print(publicKeyString) // Something along the lines of "MIGfMA0GCSqGSIb3DQEBAQUAA..."
            return publicKeyString
            // Data transmission of public key to the other party
        }
        return nil
//        guard let secKey = RSASwiftGenerator.shared.getPublicKeyReference() else { return nil }
//        let publicKey = try! PublicKey(reference: secKey)
//        return try! publicKey.base64String()
    }
    
//    static func privateKey() -> String? {
//        guard let secKey = RSASwiftGenerator.shared.getPrivateKeyReference() else { return nil }
//        let publicKey = try! PrivateKey(reference: secKey)
//        return try! publicKey.base64String()
//    }
    
    static func generateKeyPairsIfNeeded(_ completion: @escaping EmptyBlock) {
//        let keyPair = try! SwiftyRSA.generateRSAKeyPair(sizeInBits: 1024)
//        let privateKey = try! keyPair.privateKey.base64String()
//        let publicKey = try! keyPair.publicKey.base64String()
        
//        let cryptoPublicKey = try! CryptorRSA.createPublicKey(withBase64: publicKey)
//        cryptoPublicKey.type
//        CryptorRSA.
//        print(cryptoPublicKey)
        
        
//        keyPair.privateKey.reference
//        print(try! publicKey.data().bytes)
//        print(try! publicKey.base64String())
//        print(try! privateKey.base64String())
//        let sec = SecCertificate()
        
        RSASwiftGenerator.shared.getPrivateKeyReference()
        
        
        let localHeimdall = Heimdall(tagPrefix: "com.development.mti")
        if let heimdall = localHeimdall, let publicKeyData = heimdall.publicKeyDataX509() {
            print(heimdall.privateKeyData()?.base64EncodedString())
            var publicKeyString = publicKeyData.base64EncodedString()
            
            // If you want to make this string URL safe,
            // you have to remember to do the reverse on the other side later
//            publicKeyString = publicKeyString.replacingOccurrences(of: "/", with: "_")
//            publicKeyString = publicKeyString.replacingOccurrences(of: "+", with: "-")
            
            print(publicKeyString) // Something along the lines of "MIGfMA0GCSqGSIb3DQEBAQUAA..."
            
            // Data transmission of public key to the other party
        }
        
//        kRSASwiftGeneratorApplicationTag = "com.development.mti"
//        kRSASwiftGeneratorSecPadding = .PKCS1SHA1
//        kRSASwiftGeneratorKeySize = 1024
//
//        if RSASwiftGenerator.shared.keyPairExists() {
//            RSASwiftGenerator.shared.deleteSecureKeyPair(nil)
//            completion()
//        } else {
//            RSASwiftGenerator.shared.createSecureKeyPair() { (succes,error) in
//                print(succes)
//                completion()
//            }
//        }
    }
    
    static func encryptAES(message: String, key: String) -> String {
        let keyData = Data.init(bytes: md5(string: key))
        let encrypetedData = MTICipher.encrypt(message, key: keyData)//ecn(message, key: keyData)
        return encrypetedData.base64EncodedString()
    }
    
    static func generateSignature(message: String) -> String {
        let localHeimdall = Heimdall(tagPrefix: "com.development.mti")
        return localHeimdall?.signSHA1(message) ?? ""
//        return localHeimdall?.sign(message, urlEncode: false) ?? ""
    }
    
    static func md5(string: String) -> Array<UInt8> {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        return digest
    }
    
    
}

