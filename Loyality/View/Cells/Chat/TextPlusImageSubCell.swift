//
//  TextPlusImageSubCell.swift
//  Loyality
//
//  Created by Dima Solop on 8/6/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

class TextPlusImageSubCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var avatarRight: UIImageView!
    @IBOutlet weak var avatarLeft: UIImageView!
    
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var heightRewardLabel: NSLayoutConstraint!

    @IBOutlet weak var imageAswer: UIImageView!
    @IBOutlet weak var bottomImageAnswer: NSLayoutConstraint!
    @IBOutlet weak var heightImageAnswer: NSLayoutConstraint!
    
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var answerLabel2: UILabel!
    @IBOutlet weak var bottomAnswerLabel2: NSLayoutConstraint!
    
    @IBOutlet weak var imageState: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    
    var imageWidth: CGFloat?
    var imageCrop: CGFloat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageWidth = imageAswer.bounds.width
    }
    
    enum StatusImage: String {
        case cross =       "red_cross"
        case checkmark =   "green_checkmark"
        case uah =         "yelow_uah"
    }
    
    enum StatusText: String {
        case sended =       "Отправлено"
        case accepted =     "Принято"
        case paid =         "Оплачено"
        case rejected =     "Отклонено"
        case rearranged =   "Перевыставлено"
        case overdue =      "Просрочено"
        case doneByOthers = "Выполнено другими"
        case fraud =        "Мошенничество"
    }

    func setupCell(message: ChatEntry) {
        
        ///Set up date label
        dateLabel.text = message.dateAndTime
        
        ///Set up reward label
        let textRewardLabel = "  Вознаграждение: "
        let taskCostType = message.taskCoastType == 1 ? "%  " : "  "
        rewardLabel.layer.cornerRadius = heightRewardLabel.constant / 2
        rewardLabel.text = textRewardLabel + String(message.taskCost) + taskCostType
        
        ///Set up avatars
        if let urlString = message.avatar {
            avatarLeft.sd_setImage(with: URL(string: urlString), completed: nil)
        } else {
            avatarLeft.image = UIImage.init(named: "avatar_bg")
        }
        avatarRight.isHidden = true
        
        ///Set up image aswer and answerLabel2
        if message.answer2.contains(".jpg") {
            answerLabel2.text = ""
            bottomAnswerLabel2.constant = 0
            bottomImageAnswer.constant = 8
            imageAswer.isHidden = false
            imageAswer.sd_setImage(with: URL(string: message.answer2)) { (image, error, imageCash, url) in
                self.imageCrop = image?.getCropRatio()
            }
            if let crop = imageCrop, let width = imageWidth {
                heightImageAnswer.constant = width / crop
            }
        } else {
            bottomAnswerLabel2.constant = 8
            bottomImageAnswer.constant = 0
            heightImageAnswer.constant = 0
            imageAswer.isHidden = true
            answerLabel2.text = message.answer2
        }
        
        ///Set up answerLabel
        answerLabel.text = message.answer
        
        ///Set up state label and image
        switch message.status {
        case 0:
            stateLabel.text = StatusText.sended.rawValue
            imageState.image = UIImage(named: StatusImage.cross.rawValue)
        case 1:
            stateLabel.text = StatusText.accepted.rawValue
            imageState.image = UIImage(named: StatusImage.checkmark.rawValue)
        case 2:
            stateLabel.text = StatusText.paid.rawValue
            imageState.image = UIImage(named: StatusImage.uah.rawValue)
        case 3:
            stateLabel.text = StatusText.rejected.rawValue
            imageState.image = UIImage(named: StatusImage.cross.rawValue)
        case 4:
            stateLabel.text = StatusText.rearranged.rawValue
            imageState.image = UIImage(named: StatusImage.cross.rawValue)
        case 5:
            stateLabel.text = StatusText.overdue.rawValue
            imageState.image = UIImage(named: StatusImage.cross.rawValue)
        case 6:
            stateLabel.text = StatusText.doneByOthers.rawValue
            imageState.image = UIImage(named: StatusImage.cross.rawValue)
        case 7:
            stateLabel.text = StatusText.fraud.rawValue
            imageState.image = UIImage(named: StatusImage.cross.rawValue)
        default:
            return
        }
        
        
    }
    
}
