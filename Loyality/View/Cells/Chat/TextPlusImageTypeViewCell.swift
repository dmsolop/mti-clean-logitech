//
//  TextPlusImageTypeViewCell.swift
//  Loyality
//
//  Created by Dima Solop on 7/11/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import LTHRadioButton
import PureLayout

class TextPlusImageTypeViewCell: UITableViewCell {
    
    private enum TaskType: String {
        case checkList = "CHECK_LIST"
        case text = "TEXT"
        case image = "IMAGE"
        case textImage = "TEXT_IMAGE"
    }
    
    enum StatusImage: String {
        case cross =       "red_cross"
        case checkmark =   "green_checkmark"
        case uah =         "yelow_uah"
    }
    
    enum StatusText: String {
        case sended =       "Отправлено"
        case accepted =     "Принято"
        case paid =         "Оплачено"
        case rejected =     "Отклонено"
        case rearranged =   "Перевыставлено"
        case overdue =      "Просрочено"
        case doneByOthers = "Выполнено другими"
        case fraud =        "Мошенничество"
    }
    
    @IBOutlet weak var avatarLeft: UIImageView!
    @IBOutlet weak var avatarRight: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var heightTextField: NSLayoutConstraint!
    @IBOutlet weak var bottomTextField: NSLayoutConstraint!
    
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var heightStateLabel: NSLayoutConstraint!
    @IBOutlet weak var imageState: UIImageView!
    @IBOutlet weak var bottomStateLabel: NSLayoutConstraint!
    
    @IBOutlet weak var actionBtnTitle: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var heightActionBtn: NSLayoutConstraint!
    @IBOutlet weak var bottomActionBtn: NSLayoutConstraint!
    
    @IBOutlet weak var openCloseList: UIButton!
    @IBOutlet weak var heightOpenCloseBtn: NSLayoutConstraint!
    @IBOutlet weak var bottomOpenCloseBtn: NSLayoutConstraint!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var heightMainImage: NSLayoutConstraint!
    @IBOutlet weak var bottomMainImage: NSLayoutConstraint!
    
    @IBOutlet weak var checkboxStackview: UIStackView!
    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet weak var bottomCheckbox: NSLayoutConstraint!
    @IBOutlet weak var heightCheckbox: NSLayoutConstraint!
    
    @IBOutlet weak var mainTextLabel: UILabel!
    
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var heightRewardLabel: NSLayoutConstraint!
    
    var radioButtons = [LTHRadioButton]()
    var questions = [String]()
    
    var imageWidth: CGFloat?
    var imageCrop: CGFloat?
    var isText: Bool = false
    var textAnswer: String = "" 
    
    var message: ChatEntry!
    var subMessages: [ChatEntry]!
    var isShowSubcells = false
    var myIndexPathForReuse: IndexPath!
    
    private var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageWidth = mainImage.frame.width
        textField.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    //MARK: Actions
    
    @IBAction func actionBtnClicked(_ sender: UIButton) {
        postNotificationActions()
        self.textField.text = ""
        showCheckmarkAfterTask()
    }

    @IBAction func openCloseBtnClicked(_ sender: UIButton) {
        isShowSubcells = !isShowSubcells
        postNotificationSubmessages(isShowSubcells)
        let nameImageBtn = isShowSubcells ? "arrow_up" : "arrow_down"
        openCloseList.setImage(UIImage(named: nameImageBtn), for: .normal)
    }
    
    //MARK: - Notifications
    func postNotificationActions() {
        NotificationCenter.default.post(.init(name: .showActionSheet, object: nil, userInfo: ["message":message, "textAnswer": textAnswer]))
    }
    
    func postNotificationSubmessages(_ isSow: Bool) {
        NotificationCenter.default.post(.init(name: .showHideSubcells, object: nil, userInfo: ["indexPath":getSelfIndexPath(), "isShow":isSow, "subMessages":subMessages]))
    }
    
    func getSelfIndexPath() -> IndexPath {
        var myIndexPath: IndexPath!
        self.indexPath.flatMap { myIndexPath = $0 }
        return myIndexPath
    }
    
    //MARK: - CheckBox
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        if let sender = sender {
            for btn in radioButtons {
                if btn.tag == sender.view?.tag {
                    btn.select(animated: true)
                    if let subviews = sender.view?.subviews {
                        for item in subviews {
                            if item .isKind(of: UILabel.self) {
                                self.textAnswer = (item as! UILabel).text!
                            }
                        }
                    }
                    
                } else {
                    btn.deselect(animated: true)
                }
            }
        }
    }
    
    func setupCheckboxView() {
        
        let widthForLabel = checkboxStackview.frame.width
        
        for (index, item) in questions.enumerated() {
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            tapGesture.delegate = self as UIGestureRecognizerDelegate
            let radioButton = LTHRadioButton(diameter: 20, selectedColor: UIColor.white, deselectedColor: UIColor.white)
            let label = UILabel()
            let view1 = UIView()
            
            view1.backgroundColor = UIColor.clear
            
            label.text = String(item)
            label.textAlignment = .left
            label.textColor = UIColor.white
            label.font = UIFont(name: "Helvetica Neue", size: 15)
            label.numberOfLines = 2
            label.lineBreakMode = .byWordWrapping
            
            label.tag = index+1
            view1.tag = index+1
            radioButton.tag = index+1
            
            radioButtons.append(radioButton)
            
            view1.addSubview(radioButton)
            view1.addSubview(label)
            view1.addGestureRecognizer(tapGesture)
            view1.isUserInteractionEnabled = true
            
            radioButton.autoPinEdge(toSuperviewEdge: .left, withInset: 4)
            radioButton.autoPinEdge(toSuperviewEdge: .top, withInset: 12)
//            radioButton.autoAlignAxis(.horizontal, toSameAxisOf: view1, withOffset: 0)
                        
            label.autoAlignAxis(.horizontal, toSameAxisOf: view1, withOffset: 0)
            label.autoPinEdge(toSuperviewEdge: .right)
            label.autoSetDimension(ALDimension.width, toSize: widthForLabel - 30)
            
            checkboxStackview.addArrangedSubview(view1)
        }
    }
    
    ///Set up cell
    func setupCell(message: ChatEntry) {
        
        self.message = message
        
        ///Set up checkbox view
        if let quess = message.questions?.components(separatedBy: "###") {
            for item in quess {
                if item != "" {
                    self.questions.append(item)
                }
            }
        }

        if self.questions.count != 0 {
            self.heightCheckbox.constant = CGFloat(self.questions.count * 45)
            self.bottomCheckbox.constant = 8
            self.setupCheckboxView()
        } else {
            self.heightCheckbox.constant = 0
            self.bottomCheckbox.constant = 0
        }
        
        ///Set up reward label
        let textRewardLabel = "  Вознаграждение: "
        let taskCostType = message.taskCoastType == 1 ? "%  " : "  "
        rewardLabel.layer.cornerRadius = heightRewardLabel.constant / 2
        rewardLabel.text = textRewardLabel + String(message.taskCost) + taskCostType
        
        ///Set up openCloseButton
        subMessages = message.list
        if subMessages.count > 0 {
            openCloseList.isHidden = false
            heightOpenCloseBtn.constant = 44
            bottomOpenCloseBtn.constant = 8
        } else {
            openCloseList.isHidden = true
            heightOpenCloseBtn.constant = 0
            bottomOpenCloseBtn.constant = 0
        }

        ///Set up main text label
        mainTextLabel.text = message.text
        
        ///Set up avatars
        if let urlString = message.avatar {
            avatarLeft.sd_setImage(with: URL(string: urlString), completed: nil)
        } else {
            avatarLeft.image = UIImage.init(named: "avatar_bg")
        }
        avatarRight.isHidden = true
        
        
        ///Set up main image
        mainImage.sd_setImage(with: URL(string: message.imageUrl ?? "")) { (image, error, casheImage, url) in
            
                if let err = error {
                    print(err)
                    self.mainImage.isHidden = true
                    self.heightMainImage.constant = 0
                    self.bottomMainImage.constant = 0
                } else {
                    self.imageCrop = image?.getCropRatio()
                    self.mainImage.isHidden = false
                    if let crop = self.imageCrop, let width = self.imageWidth {
                        self.heightMainImage.constant = width / crop
                    }
                    self.bottomMainImage.constant = 8
                }
            }
        
        ///Set up textField
        switch message.taskType {
        case TaskType.text.rawValue,
             TaskType.textImage.rawValue:
            self.textField.isHidden = false
            self.heightTextField.constant = 44
            self.bottomTextField.constant = 8
        default:
            self.textField.isHidden = true
            self.heightTextField.constant = 0
            self.bottomTextField.constant = 0
        }
        
        ///Set up state label and image
        if message.status != 0 {
            stateLabel.isHidden = false
            heightStateLabel.constant = 40
            bottomStateLabel.constant = 8
            
            switch message.status {
            case 0:
                stateLabel.text = StatusText.sended.rawValue
                imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 1:
                stateLabel.text = StatusText.accepted.rawValue
                imageState.image = UIImage(named: StatusImage.checkmark.rawValue)
            case 2:
                stateLabel.text = StatusText.paid.rawValue
                imageState.image = UIImage(named: StatusImage.uah.rawValue)
            case 3:
                stateLabel.text = StatusText.rejected.rawValue
                imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 4:
                stateLabel.text = StatusText.rearranged.rawValue
                imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 5:
                stateLabel.text = StatusText.overdue.rawValue
                imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 6:
                stateLabel.text = StatusText.doneByOthers.rawValue
                imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 7:
                stateLabel.text = StatusText.fraud.rawValue
                imageState.image = UIImage(named: StatusImage.cross.rawValue)
            default:
                return
            }
        } else {
            stateLabel.isHidden = true
            heightStateLabel.constant = 0
            bottomStateLabel.constant = 0
        }

        ///Set up action button
        if let bText = message.buttonText, message.status == 0 {
            actionBtn.isHidden = false
            actionBtnTitle.isHidden = false
            actionBtnTitle.text = bText
            actionBtnTitle.cornerRadius = 6
            heightActionBtn.constant = 44
            bottomActionBtn.constant = 8
            layoutIfNeeded()
        } else {
            actionBtn.isHidden = true
            actionBtnTitle.isHidden = true
            heightActionBtn.constant = 0
            bottomActionBtn.constant = 0
        }
        
        ///Set up date label
        dateLabel.text = message.dateAndTime
    
    }
    
    func showCheckmarkAfterTask() {
        
        stateLabel.isHidden = false
        heightStateLabel.constant = 40
        bottomStateLabel.constant = 8
        
        actionBtn.isHidden = true
        actionBtnTitle.isHidden = true
        heightActionBtn.constant = 0
        bottomActionBtn.constant = 0
        
        stateLabel.text = StatusText.sended.rawValue
        imageState.image = UIImage(named: StatusImage.checkmark.rawValue)
        
//        timer?.invalidate()
//        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
//            <#code#>
//        })
    }
}

extension TextPlusImageTypeViewCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if self.textField.text!.count > 0 {
//            self.textAnswer = self.textField.text!
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.textAnswer = textField.text! + string
        
        return true
    }

}
