//
//  CardCollectionViewCell.swift
//  Loyality
//
//  Created by Denis Romashov on 4/9/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var code128: UIImageView!
    @IBOutlet weak var barCodeNumbers: UILabel!
    
}
