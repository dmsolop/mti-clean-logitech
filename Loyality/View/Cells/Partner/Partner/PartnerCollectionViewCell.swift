//
//  PartnerCollectionViewCell.swift
//  Loyality
//
//  Created by Denis Romashov on 4/6/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

class PartnerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var logoBackgroundView: UIView!
    @IBOutlet weak var circleView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
